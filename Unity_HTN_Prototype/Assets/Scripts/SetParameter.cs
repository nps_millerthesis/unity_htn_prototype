using UnityEngine;

namespace BehaviorDesigner.Runtime.Tasks.Basic.SharedVariables
{
    [TaskCategory("Basic/SharedVariable")]
    [TaskDescription("Sets the parameter at the given index to the specified shared variable. Returns Success.")]
    public class SetParameter : Action
    {
        [Tooltip("The value to set the parameter to")]
        public SharedVariable parameterValue;
        [RequiredField]
        [Tooltip("The task with the parameter to set")]
        public BehaviorReference referencedTask;
        [RequiredField]
        [Tooltip("The index of the parameter to set")]
        public int parameterIndex;

        public override TaskStatus OnUpdate()
        {
            NamedVariable nv = new NamedVariable();
            nv.value = (SharedVariable)parameterValue;
            referencedTask.variables[parameterIndex].Value = nv;

            return TaskStatus.Success;
        }

        
    }
}