﻿using UnityEngine;
using System.Collections.Generic;

public class AgentState : MonoBehaviour {

    // Credit to the Unite 14 Surival Shooter tutorial for inspiration, namely video 7 of 10
    public float startingHealth = 100f;
    public float currentHealth;
    public bool showDeathSpot;
    public GameObject deathSpot;

    // Enumeration for agent types
    public enum AgentTypes { Soldier, Vehicle, Aircraft, Ship};
    public AgentTypes myType;

    // Vars for health bar:
    private GameObject healthBar;
    private Renderer hbRend;
    // Store the relative scale now to normalize it's shrinkage later
    private float hbScale;

    // Use this for initialization
    void Start () {
        currentHealth = startingHealth;

        // Get a references to this GO's health bar... shorter methods were giving weird results.
        List<GameObject> findHealthBar = gameObject.GetChildren();
        foreach (GameObject child in findHealthBar)
        {
            if (child.name == "HealthBar")
            {
                healthBar = child;
                break;
            }
        }

        // To protect against healthBars being toggled off:
        if (healthBar != null) {
            hbRend = healthBar.GetComponent<Renderer>();
            hbScale = healthBar.transform.localScale.y;
        }
        
    }
	
	void Update () {
        // Remember: Update function is not extended in inheritence. 
        
	}

    /// <summary>
    /// Shouldn't be used at all, since all child classes have this method overriden.
    /// Keep this simple so that child classes can override it without needing to duplicate code.
    /// ApplyDamage(float amount) contains common code. Receive Damage is typically overriden
    /// and therefore called from the child class. This is useful for mounted infantry, for example.
    /// </summary>
    /// <param name="amount"></param>
    public virtual void ReceiveDamage(float amount) {

        ApplyDamage(amount);

    }

    protected void ApplyDamage(float amount) {

        currentHealth -= amount;

        Debug.Log(gameObject.name + " current health is " + currentHealth);
        if (healthBar != null)
        {
            // Update color with a bias towards red in the lerp
            var newColor = Color.Lerp(Color.red, new Color(0.0f,0.2f,0.0f), currentHealth / 100);
            hbRend.material.color = newColor;
            // Update localScale
            // hbScale constant normalizes for the relative scale of the healthBar compared to the parent GO
            healthBar.transform.localScale -= new Vector3(0, amount * hbScale / 100, 0);
            // Update localPosition
            healthBar.transform.localPosition -= new Vector3(0, amount / 200, 0);
        }

        if (currentHealth <= 0)
        {
            // Remove reference to all children with same tag. This is only useful when using transform hierarchies for formations.
            RemoveChildren();
            Death();
        }
    }

    protected void RemoveChildren() {

        // Kick out all the children soldiers to the parent of the current GO.
        //Debug.Log("gameobject: " + gameObject.name + " is trying to remove children with the same tag.");

        // Using a method from the "ChildGetter" class. Pain in the rear to do it in each script each time it's needed.
        List<GameObject> myChildren = ChildGetter.GetChildren(gameObject);
        foreach (GameObject child in myChildren) {
            if (child.tag == gameObject.tag) {
                child.transform.SetParent(gameObject.transform.parent);
            }
        }
    }

    protected void Death() {

        // Drop a death marker 
        if (showDeathSpot) {
            if (deathSpot != null)
            {
                // Debug.LogError("Death spot marker functionality not available on " + gameObject.name);
                float xPos = transform.position.x;
                float zPos = transform.position.z;
                //TODO: Terrain snapping:
                float yPos = 0.01f;
                Vector3 deathspotPos = new Vector3(xPos, yPos, zPos);
                // Get an instance of the Deathspot prefab, put it at the current GO's position, snapped to the terrain
                GameObject deathSpotInstance = (GameObject)Instantiate(deathSpot, deathspotPos, transform.rotation);
                // Make the agent the child of the deathspot, so when we disable the agent GO, the deathspot doesn't disappear
                // This also helps us identify which agent died where.
                deathSpotInstance.transform.parent = transform.parent;
                transform.parent = deathSpotInstance.transform;
            }
            else {
                Debug.LogError("Death spot marker null on " + gameObject.name);
            }
        }
        // Lastly, de-activate the agent gameobject
        gameObject.SetActive(false);
    }

    public AgentTypes getType() {
        AgentTypes holderType = myType;
        return holderType ;
    }

    public float getHealth() {
        return currentHealth;
    }

}