﻿using UnityEngine;
using System.Collections.Generic;

public class SoldierState : AgentState {


    /// Used when assessing damage, based on the loaded status and munition type
    /// Needs to be manually set to true if agent is starting scenario loaded
    public bool isMounted = false;


    // Use this for initialization
    void Awake () {
        myType = AgentTypes.Soldier;

    }
	
	void Update () {
        
    }

    /// <summary>
    /// Inflicts damage based on the input amount from the Detonate() method of munition or otherwise.
    /// Cuts damage in half if entity is mounted (this is arbitrary for now). Helps ensure that our mounted troops
    /// don't die when the vehicle dies--although unrealistic, the death of both makes our prototyping environment too fragile.
    /// </summary>
    /// <param name="amount"></param>
    public override void ReceiveDamage(float amount)
    {
        if (isMounted) {
            amount = amount / 2;
        }

        ApplyDamage(amount);
    }

    public void setIsMounted(bool val) {
        isMounted = val;
    }

}