﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ChildGetter
{
    //Kudos to KelsoMRK in the Unity3D forums: http://forum.unity3d.com/threads/hiow-to-get-children-gameobjects-array.142617/

    public static List<GameObject> GetChildren(this GameObject go)
    {
        List<GameObject> children = new List<GameObject>();

        if (go == null) { return children; }

        foreach (Transform tran in go.transform)
        {
            children.Add(tran.gameObject);
        }
        return children;
    }
}
