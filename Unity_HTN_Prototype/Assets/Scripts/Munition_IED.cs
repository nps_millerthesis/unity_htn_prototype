﻿using UnityEngine;
using System.Collections;

public class Munition_IED : Munition {

    /// <summary>
    /// Range out to which 100% of possible damage is inflicted. Useful for AOI-based proximity IEDs, because we want the first
    /// agent who trips the IED to receive max damage so that they die and we can test the robustness of our behaviors--for example
    /// handing a Behavior Designer Behavior Tree off to the Second in Command.
    /// </summary>
    public float maxDamageReach = 5.0f;

    public float maxDamageAmount = 100.0f;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnTriggerEnter(Collider other) {
        if (other.tag == targetTag) {
            Detonate(maxDamageReach, maxDamageAmount);
        }
    }
    /*public void iedDamage() {
        this.Detonate
    }*/
}
