﻿using UnityEngine;
using System.Collections;

public class goToStore_ChildMaker : MonoBehaviour {

    public string parentTag = "blueTarget";

    void OnTriggerEnter(Collider other) {

        // Set the new parent to be in the same position and orientation as the vehicle, and then
        // the behavior from the new parent (in this case, the BlueSoldier) will "drive" the car.

        if (other.transform.tag == parentTag) {
            other.transform.position = transform.position;
            other.transform.rotation = transform.rotation;
            transform.SetParent(other.transform);
        }
  
    }

}
