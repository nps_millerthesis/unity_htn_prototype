﻿using UnityEngine;
using System.Collections.Generic;

public class Munition : MonoBehaviour {

    public enum targetTagTypes {blueTarget, redTarget};
    /// <summary>
    /// TargetTag screens for only the GameObjects in the scene which have that tag.
    /// This helps screen out trying to "damage" inanimate objects that shouldn't be damaged,
    /// and which might cause bugs when trying to reference an attached AgentState script that doesn't exist.
    /// </summary>

    public targetTagTypes myTargetTag;
    protected string targetTag;  

    /// <summary>
    /// Max radius at which damage will be assessed. 
    /// Does not yet consider line of sight to the object being damaged.
    /// </summary>
    public float maxDamageRange = 12.0f;

    private ParticleSystem explosion;
    private AudioSource boom;

	// Use this for initialization
	void Awake () {
        // Save this reference to play the explosion when the munition detonates
        explosion = GetComponentInChildren<ParticleSystem>();

        boom = GetComponent<AudioSource>();

        targetTag = myTargetTag.ToString();
    }
	
	// Update is called once per frame
	void Update () {
	    
	}

    public void Detonate(float maxDamageZone, float maxDamage) {

        // Detonation particle system.
        if (explosion != null) {
            explosion.Play();
        }

        // Play explosion sound.
        if (boom != null)
        {
            boom.Play();
        }

        // Get all blue combatants within a certain distance:
        GameObject[] allCombatants = GameObject.FindGameObjectsWithTag(targetTag);

        foreach (GameObject combatant in allCombatants)
        {
            // Check to see if they're within range:
            float distanceFromDet = Vector3.Distance(combatant.transform.position, gameObject.transform.position);
            if (distanceFromDet <= maxDamageRange)
            {

                // Get a reference to the AgentState script. Note: this works with inheritance no problem.
                var combatantScript = combatant.GetComponent<AgentState>();

                // Then assess damage based on whether it's a vehicle or a person:
                //var enemyType = combatantScript.agentType;

                float damageAmount;
                if (distanceFromDet <= maxDamageZone)
                {
                    // Killed
                    damageAmount = maxDamage;
                }
                else 
                {
                    // Determine percentage of maxDamage to apply:
                    damageAmount = maxDamage*(1-(distanceFromDet-maxDamageZone)/(maxDamageRange-maxDamageZone));
                }

                //Debug.Log(combatant.name + " damageAmount = " + damageAmount);
                combatantScript.ReceiveDamage(damageAmount);

            }
        }

        // Deactivate the IED GO so it doesn't keep killing:
        //gameObject.SetActive(false);
        // Turn off the collider instead--same effect, but lets the particle system and audio source work:
        var myCollider = gameObject.GetComponent<Collider>();
        // Not all munitions have colliders--IDF doesn't need them. So check here:
        if (myCollider != null) {
            myCollider.enabled = false;
        }
    }

    /// <summary>
    /// Assesses damage based on:
    /// 1. Range to the target (currently a linear relationship)
    /// 2. Type of target (based on enum AgentTypes in AgentState class)
    /// 3. State of target (specifically WRT soldiers -- mounted or dismount)
    /// </summary>
    public void AssessDamage(float distance, GameObject target) {

    }
}
