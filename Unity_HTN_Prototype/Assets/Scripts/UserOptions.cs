﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class UserOptions : MonoBehaviour {

    private Toggle ShowHealthBars;
    private Toggle ShowExplosions;
    private Toggle PlaySounds;
    private Text SimTimeDisplay;

    public bool startWith_HealthBars;
    public bool startWith_Explosions;
    public bool startWith_Sounds;

    public float SimTime = 0.0f;

    private GameObject[] allHealthBars;

    // Use this for initialization
    void Start()
    {
        allHealthBars = GameObject.FindGameObjectsWithTag("healthBar");

        // Call it initially based on the public bool... so we don't have to hurry up and click it at run time.
        Toggle[] allToggles = GameObject.FindObjectsOfType<Toggle>();
        foreach (Toggle tog in allToggles) {
            switch (tog.name) {
                case "ShowHealthBars":
                    ShowHealthBars = tog;
                    break;
                case "ShowExplosions":
                    ShowExplosions = tog;
                    break;
                case "PlaySounds":
                    PlaySounds = tog;
                    break;
                default:
                    Debug.LogError("Uncmatched case of a Toggle named: " + tog.name);
                    break;
            }
        }
        ShowHealthBars.isOn = startWith_HealthBars;
        ShowExplosions.isOn = startWith_Explosions;
        PlaySounds.isOn = startWith_Sounds;

        // Get ref to the timer text (and leave room for future GUIText items):
        Text[] allGUITexts = gameObject.GetComponentsInChildren<Text>();
        foreach (Text aGuiText in allGUITexts) {
            switch (aGuiText.name) {
                case "SimTime":
                    SimTimeDisplay = aGuiText;
                    break;
            }
        }

        if (SimTimeDisplay != null) {
            SimTimeDisplay.text = "SimTime: 0:00";
        }
    }

    // Update is called once per frame
    void Update()
    {
        SimTime += Time.deltaTime;

        var mins = Mathf.Floor(SimTime / 60);
        var secs = Mathf.Floor(SimTime % 60);

        SimTimeDisplay.text = "SimTime: " + String.Format("{0:00}:{1:00}", mins, secs);
        //SimTimeDisplay.text = "Sim Time: " + mins + ":" + secs;
    }

    /// <summary>
    /// Receives a dynamic bool from the toggle in the UserOptions canvas. Turns all healthbars on or off,
    /// acting as a toggle for that visualization. 
    /// </summary>
    /// <param name="newValue"></param>
    public void ToggleHealthBar(bool newValue) {
        if (newValue)
        {
            // Turn the HealthBars back on
            foreach (GameObject bar in allHealthBars)
            {
                bar.SetActive(true);
            }
        }
        else
        {
            // Turn the HealthBars off
            foreach (GameObject bar in allHealthBars)
            {
                bar.SetActive(false);
            }
        }
    }
    
}
