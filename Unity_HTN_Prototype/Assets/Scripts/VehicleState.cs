﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// VehicleState extends AgentState.
/// </summary>
public class VehicleState : AgentState {

    /// <summary>
    /// Used to show in the inspector which vehicles are mobility killed, which are not.
    /// </summary>
    public bool isMobilityKilled;

    // Use this for initialization
    void Awake () {
        myType = AgentTypes.Vehicle;
    }
	
	void Update () {
        
	}

    /// Death should not remove the GameObject, just kick out all the child GO's with the same tag.
    /// Could help to change the alpha component such that 'dead' vehicles are greyed out.

}