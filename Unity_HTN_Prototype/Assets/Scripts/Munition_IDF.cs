﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This script will be attached to the Munition_IDF prefab, which will be spawned within the dimensions of the
/// "GraphicalRepresentation" of the "IDF_Zone" prefab.
/// It will detonate on Start()
/// </summary>

public class Munition_IDF : Munition {

    /// <summary>
    /// Range out to which 100% of possible damage is inflicted.
    /// </summary>
    public float maxDamageReach = 5.0f;

    public float maxDamageAmount = 100.0f;

    // Time after which the GO should disappear.
    public float lingerTime = 2.0f;
    private float timer;

    // Use this for initialization
    void Start () {
        // Detonate on start--calls parent class Munition Detonate() method.
        Detonate(maxDamageReach, maxDamageAmount);
        timer = 0.0f;
    }
	
	// Update is called once per frame
	void Update () {
        if (timer >= lingerTime)
        {
            // Remove from the scene altogether
            Destroy(gameObject);
        }
        else {
            timer += Time.deltaTime;
        }
	}

}
