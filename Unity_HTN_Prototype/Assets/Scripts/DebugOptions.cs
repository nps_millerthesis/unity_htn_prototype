﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Put this script on an empty gameobject anywhere in the scene
public class DebugOptions : MonoBehaviour
{
    public bool showHealth;
    private GameObject[] allHealthBars;

    void Awake() {
        allHealthBars = GameObject.FindGameObjectsWithTag("healthBar");
    }

    void Update() {
        // This is computationally wasteful... but ok with small sims. 
        // To scale up, it may be necessary to add a GUI to the scene with a button that calls a method which toggles this variable
        if (showHealth && allHealthBars.Length>0)
        {
            foreach (GameObject go in allHealthBars)
            {
                go.SetActive(true);
            }
        }
        else {
            foreach (GameObject go in allHealthBars) {
                go.SetActive(false);
            }
        }
    }

}
