﻿using UnityEngine;
using System.Collections.Generic;

public class IDF_Zone : MonoBehaviour {

    public GameObject oneRound;
    public float fireInterval = 5.0f;
    private float roundTimer;
    public float volleyDuration = 20.0f;
    private float volleyTimer;    

    private GameObject graphicalRepresentation;
    private float xWidth_Half;
    private float zWidth_Half;

	// Use this for initialization
	void Start () {
        // Find the graphical representation GO so we can measure its dimensions
        List<GameObject> findGraphicalRepresentation = gameObject.GetChildren();
        foreach (GameObject child in findGraphicalRepresentation) {
            if (child.name == "GraphicalRepresentation") {
                graphicalRepresentation = child;
                break;
            }
        }

        // Store the x and z dimensions of the graphical representation
        xWidth_Half = graphicalRepresentation.transform.localScale.x / 2;
        zWidth_Half = graphicalRepresentation.transform.localScale.z / 2;

        roundTimer = 0.0f;
        volleyTimer = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
        // Is it time for the next round?
        if (roundTimer >= fireInterval)
        {
            // Get random x and z values for the offset within the graphicalRepresentation box
            // Random.Range provides access to a uniform distribution. Random.value is a normal distribution.
            var offsetX = (Random.Range(0.0f,1.0f) * xWidth_Half) * (Random.value*2 - 1);
            var offsetZ = (Random.Range(0.0f, 1.0f) * zWidth_Half) * (Random.value * 2 - 1);
            Vector3 offset = new Vector3(offsetX, 0.0f, offsetZ);
            Vector3 spawnIDF = new Vector3();
            spawnIDF = transform.position - offset;

            // Instantiate the mortar prefab
            Instantiate(oneRound, spawnIDF,transform.rotation);

            // Reset the timer:
            roundTimer = 0.0f;
        }
        else
        {
            roundTimer += Time.deltaTime;
        }

        // Should the volley end?
        if (volleyTimer >= volleyDuration)
        {
            Destroy(gameObject);
        }
        else {
            volleyTimer += Time.deltaTime;
        }
	}

    public void SetRoundInterval(float inteveral) {
        this.fireInterval = inteveral;
    }

    public void SetVolleyDuration(float duration)
    {
        this.volleyDuration = duration; 
    }
}
