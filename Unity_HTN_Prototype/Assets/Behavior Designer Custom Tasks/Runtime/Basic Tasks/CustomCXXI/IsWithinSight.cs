using UnityEngine;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime.Tasks.Movement;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;

namespace BehaviorDesigner.CustomCXXI
{

    [TaskCategory("CustomCXXI")]
    [TaskDescription("Check to see if a given target or any instance of target with the specified tag is within sight. This task will choose the closest target, regardless of threat. Return success and provides an option to set a SharedVariable for other nodes of the tree to use.")]
    public class IsWithinSight : Conditional
    {
        //NOTE: THIS TASK HAS NOT BEEN TESTED SUFFICIENTLY WITH JUST A GAMEOBJECT INPUT, BEST TO USE A TAG

        [Tooltip("How wide of an angle the agent can see (degrees)")]
        public float fieldOfViewAngle = 100;
        [Tooltip("How far can the agent see (unity units)")]
        public SharedFloat viewDistance = 50;
        [Tooltip("Look for this specific target and no others.")]
        public SharedGameObject specificTarget;
        [Tooltip("Look for any target with this tag, if a specific GameObject isn't specified (above).")]
        public string targetTag;
        [Tooltip("Visualize the line of sight raycast for each possible target, showing what it does hit.")]
        public bool visualizeRaycast = true;
        [Tooltip("Shared Target to use in other tasks.")]
        public SharedGameObject closestTarget;
        public SharedTransform closestTargetTransform;

        // A cache of all of the possible targets
        private Transform[] possibleTargets;

        public override void OnAwake()
        {
            // set targetTag to the opposite of this GO's tag.
            var myTag = gameObject.tag;
            switch (myTag) {
                case "blueTarget":
                    targetTag = "redTarget";
                    break;
                case "redTarget":
                    targetTag = "blueTarget";
                    break;
            }

            // Get a GO array of all the possible enemy targets:
            var targets = GameObject.FindGameObjectsWithTag(targetTag);
            possibleTargets = new Transform[targets.Length];
            for (int i = 0; i < targets.Length; ++i)
            {
                possibleTargets[i] = targets[i].transform;
            }
        }

        public override TaskStatus OnUpdate()
        {
            float closestTargetDistance = 100000.0f; // Arbitrarily large

            // Check for matching targets within LOS parameters
            foreach(Transform targetX in possibleTargets)
            {
                //Make sure it hasn't been destroyed already.
                if (targetX != null)
                {
                    // First check that it's within the FOV and the viewDistance range; raycasting is more expensive, so do that later:
                    var direction = targetX.position - (transform.TransformPoint(Vector3.zero));
                    direction.y = 0;
                    float angle = Vector3.Angle(direction, transform.forward);
                    if (direction.magnitude < viewDistance.Value && angle < fieldOfViewAngle * 0.5f)
                    {
                        // If it has LOS
                        RaycastHit hit;
                        Physics.Raycast(gameObject.transform.position, targetX.position - gameObject.transform.position, out hit);
                        if (hit.transform != null && (hit.transform.tag == targetTag || hit.transform.name == specificTarget.Name))
                        {
                            // If it is the closest thus far
                            if ((direction.magnitude < closestTargetDistance))
                            {
                                // Assign it as closestTarget;
                                closestTargetDistance = Vector3.Distance(gameObject.transform.position, targetX.position);
                                closestTarget.SetValue(targetX.gameObject); // = targetX;
                                closestTargetTransform.SetValue(targetX);
                                // Return success later, after we've gone through all the possible targets. Only return the closest target.
                            }
                            //For LOS visualization:
                            if (visualizeRaycast)
                            {
                                Debug.DrawLine(transform.position, hit.transform.position, Color.yellow);
                            } //else draw nothing...
                        }
                    }
                }
            }

            if (closestTarget.Value == null)
            {
                // Found target within parameters
                //Debug.Log("closest target is " + closestTarget.Value.name);
                return TaskStatus.Failure;
            }
            else {
                return TaskStatus.Success;
            }
        }
        // Draw the line of sight representation within the scene window
        public override void OnDrawGizmos()
        {
            MovementUtility.DrawLineOfSight(Owner.transform, Vector3.zero, fieldOfViewAngle, viewDistance.Value, false);
        }

        // Must reset the values so that if the target goes out of sight, it isn't still a valid target.
        public override void OnReset()
        {
            //closestTarget = new SharedTransform();
        }
    }
}
