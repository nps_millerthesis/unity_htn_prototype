using UnityEngine;
using System.Collections.Generic;

namespace BehaviorDesigner.Runtime.Tasks.Basic.UnityGameObject
{
    [TaskCategory("CustomCXXI")]
    [TaskDescription("Instantiates a new IDF_Zone--a box which generates Munition_IDF prefabs at a set interval within the box. Returns Success.")]
    public class CallForFire : Action
    {
        [Tooltip("The Prefab that the task operates on. If null the task GameObject is used.")]
        public SharedGameObject IDF_Prefab;
        [Tooltip("Use any GameObject to mark the center point.")]
        public SharedGameObject IDF_Marker;
        private Quaternion zoneRotation = Quaternion.identity;
        [Tooltip("Scale is particularly important--X a Z only. Y will be kept at 0.001")]
        // This must find the child GraphicalRepresentation and scale that--not the parent GO.
        public SharedVector3 scale;
        [Tooltip("Determines the rate of fire. Default is once every 5 seconds.")]
        public float fireInterval = 5.0f;
        [Tooltip("Determines how long the IDF_Zone will exist. It is destroyed after this time.")]
        public float volleyDuration = 20.1f;

        private GameObject IDF_Zone;
        private GameObject graphicalRepresentation;

        public override TaskStatus OnUpdate()
        {
            GameObject idfZone = Object.Instantiate(IDF_Prefab.Value, IDF_Marker.Value.transform.position, zoneRotation) as GameObject;

            // Set the round interval and volley duration:
            var idfScript = idfZone.GetComponent<IDF_Zone>();
            idfScript.SetRoundInterval(fireInterval);
            idfScript.SetVolleyDuration(volleyDuration);

            // Find the graphical representation GO so we can set its dimensions
            List<GameObject> findGraphicalRepresentation = idfZone.GetChildren();
            foreach (GameObject child in findGraphicalRepresentation)
            {
                if (child.name == "GraphicalRepresentation")
                {
                    graphicalRepresentation = child;
                    break;
                }
            }
            scale.Value.Set(scale.Value.x, 0.001f, scale.Value.z);
            if (graphicalRepresentation != null) {
                graphicalRepresentation.transform.localScale = scale.Value;
            }

            return TaskStatus.Success;
        }

        public override void OnReset()
        {
            IDF_Prefab = null;
            IDF_Marker = null;
            zoneRotation = Quaternion.identity;
        }
    }
}