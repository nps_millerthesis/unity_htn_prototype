using UnityEngine;

namespace BehaviorDesigner.Runtime.Tasks.Basic.UnityDebug
{
    [TaskCategory("CustomCXXI")]
    [TaskDescription("Requires a SharedTransform of a designated target, and shoots at the target until it's no longer active in the scene.")]
    public class EngageTarget : Action
    {
        [Tooltip("The manually specified enemy")]
        public GameObject target;
        [Tooltip("The designated enemy's transform, should come from a detection task (i.e. isWithinSight)")]
        public SharedTransform myTarget;
        public float timeBetweenShots = 0.5f;
        public SharedInt damageRate;
        private float lastFire = 0.0f;
        [Tooltip("Show or not show the line (only in the scene view) during firing.")]
        public bool visualization = true;
        
        //Private line-drawing variables:
        private Transform start;     //SharedVector3
        private Transform end;       //SharedVector3
        private SharedColor color = Color.red;
        private float fireVisDuration; //This allows the "fire" line to linger longer than a single frame so you can actually see it

        // Set to automatically populate. All potential targets must have an instance of Soldier_GeneralAttributes.
        private AgentState otherScript;


        // Use this for initialization
        void Start()
        {
            fireVisDuration = timeBetweenShots/5; // So it should "linger" for about 0.1 seconds after the shot is fired
        }

        public override TaskStatus OnUpdate()
        {
            // This task requires that either a target GO or transform be provided to it, otherwise, failure
            // If it hasn't been established in the Start method, then it ain't happening, and so return failure here
            //              can't return failure in the Start method, although it would make more sense
            if (myTarget == null) {
                Debug.Log("DamageTarget task requires a target");
                return TaskStatus.Failure;
            }
            // Retrieve the Soldier_GeneralAttributes script from the target. This allows us to inflict damage on the target GameObject.
            if (otherScript == null) {
                otherScript = myTarget.Value.gameObject.GetComponent<AgentState>();
                //otherScript = target.GetComponent<Soldier_GeneralAttributes>();
            }

            // Continue to engage while the target is there.
            //while (myTarget.Value.gameObject.activeInHierarchy)
            //{
                float deltaTime = Time.deltaTime;
                if (lastFire >= timeBetweenShots)
                {
                    // Then FIRE!
                    otherScript.ReceiveDamage(damageRate.Value);
                    lastFire = 0.0f;
                }
                else
                {
                    lastFire += Time.deltaTime;
                }

                // If vis==true, and it's been < 0.1 seconds since the "shot", then draw the line:
                if (visualization && (lastFire - deltaTime) < fireVisDuration)
                {
                    if (!start)
                    {
                        start = transform;
                        end = myTarget.Value.transform;
                    }
                    Debug.DrawLine(start.position, end.transform.position, color.Value);
                } //else draw nothing...
            //}
            return TaskStatus.Success; //The target is dead (inactive)
        } 
    }
}