using UnityEngine;


namespace BehaviorDesigner.Runtime.Tasks.Basic.UnityGameObject
{
    [TaskCategory("CustomCXXI")]
    [TaskDescription("Destroys the GameObject after a specified amount of time, as a cheap way of emulating engagement in CXXI. It's a less functional but less buggy version of 'DamageTarget.cs'.")]
    public class TimedEngage : Action
    {
        [Tooltip("The GameObject that the task operates on. If null the task GameObject is used.")]
        public SharedGameObject targetGameObject;
        /*[Tooltip("Minimum time to destroy the GameObject")]
        public float minDelay = 3.0f;
        [Tooltip("Maximum time to destroy the GameObject")]
        public float maxDelay = 6.0f;*/
        private float delayTime = 3.0f;
        private float startTime;

        void Start() {
            // Mark start time.
            startTime = Time.time;

            // randomly generate the delay time based on the min/max provided by user.
            //actualDelay = Random.value*(maxDelay-minDelay)+minDelay;

        }

        public override TaskStatus OnUpdate()
        {
            if (targetGameObject == null) {
                return TaskStatus.Failure;
            }
            // var destroyGameObject = GetDefaultGameObject(targetGameObject.Value);
            if ((Time.time - startTime) >= delayTime) {

                GameObject.Destroy(targetGameObject.Value);

                // So that other tasks don't keep trying to access targetGameObject's non-existent value:
                //targetGameObject = null;
                return TaskStatus.Success;
            }
            return TaskStatus.Running;
        }

        public override void OnReset()
        {
            startTime = Time.time;
        }
    }
}