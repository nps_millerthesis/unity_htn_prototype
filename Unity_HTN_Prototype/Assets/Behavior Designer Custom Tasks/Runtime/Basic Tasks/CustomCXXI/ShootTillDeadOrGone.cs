using UnityEngine;

namespace BehaviorDesigner.Runtime.Tasks.Basic.UnityDebug
{
    [TaskCategory("CustomCXXI")]
    [TaskDescription("Uses target's AgentState.cs script to reduce their health by a set amount at set intervals until enemy is either out of sight or dead. Returns running if still engaging, success if enemy is dead, failure if enemy is no longer in sight.")]
    public class ShootTillDeadOrGone : Action
    {
        [Tooltip("The designated enemy's transform, should come from a detection task (i.e. isWithinSight)")]
        public SharedTransform myTarget;
        [Tooltip("May or may not be the same as the Observe task range for FOV")]
        public SharedFloat engageRange = 15f;
        public float timeBetweenShots = 0.5f;
        public SharedInt damageRate = 20;
        
        [Tooltip("Show or not show the line (only in the scene view) during firing.")]
        public bool visualization = true;
        [Tooltip("Enable quaternion-based rotation tracking during engagement.")]
        public bool faceTarget = true;

        //Variables from SurivalShooter tutorial 
        float timer;
        RaycastHit shootHit;
        //int shootableMask;
        LineRenderer shotLine;
        float shotDisplayTime = 0.2f;

        // Set to automatically populate. All potential targets must have an instance of AgentState.
        private AgentState targetAgentStateScript;

        // Use this for initialization
        public override void OnStart()
        {
            shotDisplayTime = timeBetweenShots/5; // So it should "linger" for about 0.1 seconds after the shot is fired
            targetAgentStateScript = myTarget.Value.gameObject.GetComponent<AgentState>();
            //shootableMask = LayerMask.GetMask("Default");
            shotLine = GetComponent<LineRenderer>();
        }

        public override TaskStatus OnUpdate()
        {
            if(targetAgentStateScript == null) {
                targetAgentStateScript = myTarget.Value.gameObject.GetComponent<SoldierState>();
            }
            
            //shootableMask = LayerMask.GetMask("Default");  //myTarget.Value.gameObject.layer;
            // Add the time since Update was last called to the timer.
            timer += Time.deltaTime;

            // Check the timer to see if the shot visualization should be done
            if (visualization && timer >= shotDisplayTime) {
                shotLine.enabled = false;
            }

            // This task requires that either a target GameObject or transform be provided to it, otherwise, failure
            // If it hasn't been established in the Start method, then it ain't happening, and so return failure here
            //              can't return failure in the Start method, although it would make more sense in a normal script
            if (myTarget == null) {
                Debug.LogError("DamageTarget task requires a target");
                return TaskStatus.Failure;
            }
            // Retrieve the AgentState script from the target. This allows us to inflict damage on the target GameObject.
            if (targetAgentStateScript == null)
            {
                Debug.LogError("Unable to retrieve script from " + myTarget.Value.gameObject.name + " in the ShootTillDeadOrGone script");
                return TaskStatus.Failure;
            }

            // Get raycast and distance info from agent to target for use in "else if" check below.
            float distanceToTarget = Vector3.Distance(gameObject.transform.position, myTarget.Value.position);

            // If the myTarget GameObject isn't active, assume it's been killed, return success
            if (distanceToTarget > engageRange.Value || !myTarget.Value.gameObject.activeInHierarchy || targetAgentStateScript.currentHealth <= 0)
            {
                // We're done. Either he's dead or out of range.
                //Debug.Log(gameObject.name + " killed or lost sight of " + myTarget.Value.gameObject.name);
                // Stop referrencing the dead soldier, then return success.
                myTarget.Value = null;
                //  Turnoff shot vis
                if (visualization)
                {
                    shotLine.enabled = false;
                }
                myTarget.Value = null;
                return TaskStatus.Success;
            }
            // Otherwise, keep shooting
            // If timer exceeds time between shots, check LOS and fire again
            else if (timer >= timeBetweenShots) 
            {
                RaycastHit hitinfo;
                Vector3 raycastDirection = myTarget.Value.transform.position - gameObject.transform.position;
                // Note: Physics.Raycast is heavily overloaded. Can use a multitude of other versions.
                bool hitTarget = Physics.Raycast(gameObject.transform.position, raycastDirection, out hitinfo);

                if (hitTarget && hitinfo.collider.name == myTarget.Value.gameObject.name) {
                    Attack();
                }
                return TaskStatus.Running;
            }
            else {
                // Our target is still alive, but we need to wait before shooting it again.
                return TaskStatus.Running;
            }
        }

        void Attack() {
            //Reset the timer.
            timer = 0.0f;

            //If the target still has health
            if (targetAgentStateScript.currentHealth > 0)
            {
                // Take health first
                targetAgentStateScript.ReceiveDamage(damageRate.Value);
                // Visualization of the shot second.
                // TODO: VIS NOT WORKING, SHOTLINE SEEMS OUT OF SCOPE
                if (visualization)
                {
                    // Set up the shot visualization
                    shotLine.enabled = true;
                    shotLine.SetPosition(0, transform.position);
                    shotLine.SetPosition(1, myTarget.Value.position);
                }
            }
        }
    }
}