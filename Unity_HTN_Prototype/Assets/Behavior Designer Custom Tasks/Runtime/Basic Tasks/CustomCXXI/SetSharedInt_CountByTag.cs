using UnityEngine;
using System.Collections.Generic;

namespace BehaviorDesigner.Runtime.Tasks.Basic.SharedVariables
{
    [TaskCategory("CustomCXXI")]
    [TaskDescription("Sets the SharedInt variable to the number of objects within FOV that match the given LayerMask / tag combo. Returns Success.")]
    public class SetSharedInt_CountByTag : Action
    {

        [Tooltip("The SharedInt to set")]
        public SharedInt targetVariable;
        [Tooltip("The LayerMask of the objects that we are searching for, e.g. buildings. Can be any LayerMask")]
        public LayerMask objectLayerMask;
        [Tooltip("The tag of the objects that we are searching for")]
        public string objectTag;
        [Tooltip("The field of view angle of the agent (in degrees)")]
        public SharedFloat fieldOfViewAngle = 45;
        [Tooltip("The distance that the agent can see ")]
        public SharedFloat viewDistance = 150;
        [Tooltip("The object that is within sight")]
        public SharedTransformList objectsInSight;
        [Tooltip("The number of objects within sight")]
        public SharedInt numObjectsInSight;

        public override TaskStatus OnUpdate()
        {
            // Determine if there are any objects within sight based on the layer mask

            objectsInSight.Value = Movement.MovementUtility.AllWithinTheFOVFan(transform, Vector3.zero, fieldOfViewAngle.Value, viewDistance.Value, objectLayerMask, objectTag);
            numObjectsInSight = objectsInSight.Value.Count;
            targetVariable.Value = numObjectsInSight.Value;

            // return the success
            //return objectsInSight;

            return TaskStatus.Success;
        }

        public override void OnReset()
        {
            targetVariable = 0;
        }

        // Draw the line of sight representation within the scene window
        public override void OnDrawGizmos()
        {
            Movement.MovementUtility.DrawLineOfSight(Owner.transform, Vector3.zero, fieldOfViewAngle.Value, viewDistance.Value, false);
        }
    }
}