using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace BehaviorDesigner.Runtime.Tasks.Movement
{
    [TaskDescription("Patrol along a Waypoint Route once using the Unity NavMesh.")]
    [TaskCategory("CustomCXXI")]
    [HelpURL("http://www.opsive.com/assets/BehaviorDesigner/Movement/documentation.php?id=7")]
    [TaskIcon("Assets/Behavior Designer Movement/Editor/Icons/{SkinColor}PatrolOneWayIcon.png")]
    public class PatrolOneWay : NavMeshAgentMovement
    {
        [Tooltip("The agent has arrived when the square magnitude is less than this value. This value could be expanded when waypoints are intended to be 'guidelines', and other factors will also drive movement, as in HTNs")]
        public SharedFloat arriveDistance = 1.0f;
        [Tooltip("The length of time that the agent should pause when arriving at a waypoint")]
        public SharedFloat waypointPauseDuration = 0;
        [Tooltip("The waypoints to move to, in the order of movement, regardless of spatial orientation.")]
        public SharedTransformList waypoints;
        //[Tooltip("Route to take, in order of the waypoints' listing as children. Over-writes waypoints variable (above).")]
        //public SharedTransform route;

        // The current index that we are heading towards within the waypoints array
        private int waypointIndex;
        private float waypointReachedTime;

        public override void OnStart()
        {
            base.OnStart();

            // Tried building route functionality, but gave up on that:
            /*// Check to see if there is a route:
            if (route.Value != null) {
                // Build out route of child WPs
                List<Transform> newRoute = new List<Transform>();
                List<GameObject> children = ChildGetter.GetChildren(route.Value.gameObject);
                foreach (GameObject child in children) {
                    if (child.tag == "waypoint") {
                        newRoute.Add(child.transform);
                    }
                }
                // Store it as waypoints SharedTransformList
                waypoints = newRoute;
            }*/

            // initially move towards the first waypoint regardless of relative position
            waypointIndex = 0;

            waypointReachedTime = -waypointPauseDuration.Value;
            navMeshAgent.destination = Target();
        }

        // Patrol along the ordered waypoints specified in the waypoint array. Return a task status of running until arrival at the final waypoint. 
        public override TaskStatus OnUpdate()
        {
            if (!navMeshAgent.pathPending)
            {

                if (waypointIndex < waypoints.Value.Count)
                {

                    var thisPosition = transform.position;
                    thisPosition.y = navMeshAgent.destination.y; // ignore y
                    if (Vector3.SqrMagnitude(thisPosition - navMeshAgent.destination) < arriveDistance.Value)
                    {

                        if (waypointReachedTime == -waypointPauseDuration.Value)
                        {
                            waypointReachedTime = Time.time;
                        }
                        // wait the required duration before switching waypoints.
                        if (waypointReachedTime + waypointPauseDuration.Value <= Time.time)
                        {
                            waypointIndex = waypointIndex + 1;

                            if (waypointIndex < waypoints.Value.Count)
                            {
                                navMeshAgent.destination = Target();
                                waypointReachedTime = -waypointPauseDuration.Value;
                            }
                        }
                    }
                    //return TaskStatus.Running;
                }
                else
                {
                    return TaskStatus.Success;
                }
            }
            /*else
            {
                return TaskStatus.Inactive;
            }*/
            return TaskStatus.Running;


        }

        // Return the current waypoint index position
        private Vector3 Target()
        {
            // In case the user doesn't provide any input
            if (waypoints.Value.Count == 0) {
                Debug.LogError("No waypoints read for " + gameObject.name + "PatrolOneWay");
                return gameObject.transform.position;
            }
            return waypoints.Value[waypointIndex].position;
        }

        // Reset the public variables
        public override void OnReset()
        {
            base.OnReset();

            arriveDistance = 0.1f;
            waypointPauseDuration = 0;
            waypoints = null;
        }

        // Draw a gizmo indicating a patrol 
        public override void OnDrawGizmos()
        {
#if UNITY_EDITOR
            if (waypoints.Value == null)
            {
                return;
            }
            var oldColor = UnityEditor.Handles.color;
            UnityEditor.Handles.color = Color.yellow;
            for (int i = 0; i < waypoints.Value.Count; i++)
            {
                if (waypoints.Value[i] != null)
                {
                    UnityEditor.Handles.SphereCap(0, waypoints.Value[i].position, waypoints.Value[i].rotation, 1);
                }
            }
            UnityEditor.Handles.color = oldColor;
#endif
        }
    }
}