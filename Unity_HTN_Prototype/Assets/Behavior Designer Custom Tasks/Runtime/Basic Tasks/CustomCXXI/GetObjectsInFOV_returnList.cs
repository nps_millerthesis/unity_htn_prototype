using UnityEngine;
using System.Collections.Generic;

namespace BehaviorDesigner.Runtime.Tasks.Movement
{
    [TaskDescription("Get all the objects with a specific tag, return them in a SharedTransformList. Only useful if you store the SharedTransformList in a shared variable. Returns success.")]
    [TaskCategory("CustomCXXI")]
    [HelpURL("http://www.opsive.com/assets/BehaviorDesigner/Movement/documentation.php?id=11")]
    [TaskIcon("Assets/Behavior Designer Movement/Editor/Icons/{SkinColor}CanSeeObjectIcon.png")]
    public class GetObjectsInFOV_returnList : Action
    {

        [Tooltip("The tag of the objects that we are searching for")]
        public string objectTag;
        [Tooltip("The field of view angle of the agent (in degrees)")]
        public SharedFloat fieldOfViewAngle = 45;
        [Tooltip("The distance that the agent can see ")]
        public SharedFloat viewDistance = 150;
        [Tooltip("The offset relative to the pivot position")]
        public SharedVector3 offset = Vector3.zero;
        [Tooltip("The SharedTransformList of objects with the tag in the FOV")]
        public List<Transform> objectsInSight;
        //public Transform[] objectsInSight;
        [Tooltip("Stores the number of objectsInSight")]
        public SharedInt objectsInSightCount;

        // A cache of all of the possible targets
        private List<Transform> possibleObjects;

        void Start()
        {
            objectsInSightCount = 0;
        }


        // Returns success if an object was found otherwise failure
        public override TaskStatus OnUpdate()
        {
            // Cache all of the transforms that have a tag of targetTag
            var objects = GameObject.FindGameObjectsWithTag(objectTag);
            for (int i = 0; i < objects.Length; ++i)
            {
                possibleObjects.Add(objects[i].transform);
            }

            if (possibleObjects.Count > 0)
            {
                // Check for matching targets within LOS parameters
                foreach (Transform targetX in possibleObjects)
                {
                    // First check that it's within the FOV and the viewDistance range; raycasting is more expensive, so do that later:
                    var direction = targetX.position - (transform.TransformPoint(Vector3.zero));
                    direction.y = 0;
                    float angle = Vector3.Angle(direction, transform.forward);
                    if (direction.magnitude < viewDistance.Value && angle < fieldOfViewAngle.Value * 0.5f)
                    {
                        objectsInSight.Add(targetX);
                    }
                }
            }

            return TaskStatus.Success;
        }

        // Draw the line of sight representation within the scene window
        public override void OnDrawGizmos()
        {
            MovementUtility.DrawLineOfSight(Owner.transform, offset.Value, fieldOfViewAngle.Value, viewDistance.Value, false);
        }
    }
}